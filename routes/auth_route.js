const express = require('express');
const router = express.Router();
const User = require('../models/user_schema')
const bcrypt = require('bcrypt')
const { check, validationResult } = require('express-validator')

//post auth
router.post('/auth', [
    check('password').isLength({ max: 8 })
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    let user = await User.findOne({ email: req.body.email })
    if (!user) return res.status(400).send({ message:'user or password invalid'});

    const validatePassword = await bcrypt.compare(req.body.password, user.password)
    if (!validatePassword) return res.status(400).send({ message:'user or password invalid'})

    const jwtToken = user.generateJWT();
    res.status(201).header('Authorization', jwtToken).send({

        _id: user._id,
        message: 'you have logged in',
        token: jwtToken
    });

});
module.exports = router;