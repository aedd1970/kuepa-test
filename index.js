const express = require('express');
const mongoose = require('mongoose');
const app = express()
const http = require('http')
var soketIo = require('socket.io')
var cors = require('cors')
//call route
const user = require('./routes/user_route')
const auht = require('./routes/auth_route')

app.use(express.json());
app.use(cors())
app.use('/api/register', user);
app.use('/api/login', auht);


//connection local-port
const server =http.createServer(app)

const port = process.env.PORT || 3001
server.listen(port, () => console.log('listening to the port:' + port));

//listen on socket
const io = soketIo.listen(server)
io.on('connection', (socket) => {
  console.log('new conexion id: ' + socket.id)
  // event socket
    socket.on('data_user', (data) => {
        console.log('email'+ data.email + 'usuario:' + data.user);
        io.emit('new_user', {login: data.user})
    })
})

//conexion base de datos
mongoose.connect('mongodb+srv://kuepa_user:kuepa123*@cluster0-x6ack.mongodb.net/kuepa?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify:false,
    useCreateIndex: true
})

    .then(() => console.log('Connected to the database'))
    .catch(() => console.log('Mongo db connection failed'))