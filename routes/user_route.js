const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const User = require('../models/user_schema')
const { check, validationResult } = require('express-validator')

//post user
router.post('/user', [
  check('password').isLength({ max: 8 }),
  check('password', "Please enter a password at least 8 character and contain At least one uppercase.At least one lower case.At least one special character.").matches(
    /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d@$.!%*#?&]/)
], async (req, res) => {
  const err = validationResult(req)
  if (!err.isEmpty()) {
    return res.status(422).json({ err: err.array() });
  }
  let user = await User.findOne({ email: req.body.email })

  if (user) return res.status(400).send('user already exists');

  //bcrypt
  const salt = await bcrypt.genSalt(10)
  const hashPassword = await bcrypt.hash(req.body.password, salt)

  user = new User({
    name: req.body.name,
    email: req.body.email,
    type_user: req.body.type_user,
    password: hashPassword
  })

  const result = await user.save()
  const jwtToken = user.generateJWT();

  res.status(201).header('Authorization', jwtToken).send({
    _id: user._id,
    name: user.name,
    email: user.email,
    token: jwtToken
  });
})

module.exports = router;