const jwt = require('jsonwebtoken')

var apiKey = process.env.APIKEY || '12345';
const auth  = ( req, res, next ) => {

    const jwtToken = req.header('Authorization')
    if(!jwtToken) return res.status(401).send('access denied');;
    try{
        const payload = jwt.verify(jwtToken, 'password')
        req.user = payload
        next()
    }catch(e){
        res.status(400).send('Acceso denegado. Token no valido')
    }
}
module.exports = auth