const mongoose = require ('mongoose')
const jwt = require('jsonwebtoken')

const userSchema = new mongoose.Schema({

    name:{
        type: String,
        required: true
    },
    email:{
     type: String,
     required: true,
     unique: true
    },
    password:{
        type: String,
        required: true,
    },
    isAdmin: Boolean,
    date: {
        type: Date,
        default:Date.now
    }, 
    type_user:{
        type: String,
        required: true
    }
})
userSchema.methods.generateJWT = () => {
    return jwt.sign({
        _id: this._id,
        name: this.name,
        idAdmin: this.isAdmin,
    }, 'password' )
}
const User = mongoose.model('user', userSchema)
module.exports = User